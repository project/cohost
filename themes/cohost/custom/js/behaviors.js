(function ($, Drupal) {
  Drupal.behaviors.CoHostCustomBehaviors = {
    attach: function (context, settings) {
      // Custom JS here.
    },
  };

  Drupal.behaviors.CoHostMenu = {
    attach: function (context, settings) {
      if ($("ul.navbar-nav li a.is-active")) {
        $("ul.navbar-nav li a.is-active").parent().addClass("active");
      }
    },
  };
})(jQuery, Drupal);
